/**
 * @desc electron 主入口
 */
import path from 'path';
import { app, dialog, webContents, BrowserWindow, Menu, MenuItemConstructorOptions, MenuItem, ipcMain } from 'electron';
import store, { KEYS } from './store';

function isDev() {
  // 👉 还记得我们配置中通过 webpack.DefinePlugin 定义的构建变量吗
  return process.env.NODE_ENV === 'development';
}

function createWindow() {
  // 创建浏览器窗口
  const mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    webPreferences: {
      devTools: true,
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      contextIsolation: false,
    },
  });

  if (isDev()) {
    // 👇 看到了吗，在开发环境下，我们加载的是运行在 10000 端口的 React
    mainWindow.loadURL(`http://127.0.0.1:10000`);
  } else {
    mainWindow.loadURL(`file://${path.join(__dirname, '../dist/index.html?d=${openedDocCenterPath}')}`);
  }
}

function buildMenu() {
  const template: Array<MenuItemConstructorOptions | MenuItem> = [
    {
      label: app.name,
      submenu: [
        { label: `About ${app.name}`, role: 'about' },
        { label: `Service`, role: 'services' },
        { label: `Quit`, role: 'quit' },
      ],
    },
    {
      label: 'File',
      submenu: [
        { label: 'Open Recently', role: 'recentDocuments' },
        {
          label: 'Open Document',
          accelerator: 'CmdOrCtrl+O',
          click() {
            let focused = BrowserWindow.getFocusedWindow();
            if (focused) {
              let ret = dialog.showOpenDialogSync(focused, {
                title: 'Open Doc Center',
                properties: ['openDirectory'],
              });
              if (ret && ret.length) {
                store.set(KEYS.ROOT, ret[0]);
              }
            }
          },
        },
        { label: 'Close', role: 'close' },
      ],
    },
    {
      label: 'Edit',
      submenu: [
        { label: 'Undo', role: 'undo' },
        { label: 'Redo', role: 'redo' },
      ],
    },
    {
      label: 'View',
      submenu: [
        { label: 'Reload', role: 'reload' },
        { label: 'Force Reload', role: 'forceReload' },
        { label: 'Toggle DevTools', role: 'toggleDevTools' },
        { type: 'separator' },
        { label: 'Actual Size', role: 'resetZoom' },
        { label: 'Zoom In', role: 'zoomIn' },
        { label: 'Zoom Out', role: 'zoomOut' },
        { type: 'separator' },
        { label: 'Toggel Full', role: 'togglefullscreen' },
      ],
    },
    {
      label: 'Window',
      submenu: [
        { label: 'Minimize', role: 'minimize' },
        { label: 'Zoom', role: 'zoom' },
      ],
    },
  ];
  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}

app.whenReady().then(buildMenu).then(createWindow);

app.on('activate', function () {
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

app.on('window-all-closed', () => {
  app.quit();
});
