const Store = require('electron-store');

const store = new Store({ accessPropertiesByDotNotation: false, watch: true });

export default store;

export const KEYS = {
  ROOT: 'ROOT',
};
