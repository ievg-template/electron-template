declare module '*.less' {
  const content: { [className: string]: string };
  export default content;
}

declare module '*png' {
  const png: string;
  export default png;
}
