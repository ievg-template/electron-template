import React from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import NotFound from './page/404';
import DocCenter from './page/DocCenter';
import Guide from './page/Guide';

function Router() {
  return (
    <HashRouter>
      <Switch>
        {/* 👇 一定要添加 exact */}
        <Route path={['/doc-center', '/']} exact component={DocCenter} />
        <Route path="/guide/:filename" exact component={Guide} />
        <Route path="*">
          <NotFound text="this is text" />
        </Route>
      </Switch>
      {/* 重定向到首页 */}
      <Redirect to="/" />
    </HashRouter>
  );
}
export default Router;
