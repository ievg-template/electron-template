import React from 'react';
import './index.less';
import { useHistory } from 'react-router';

import Boy from '@assets/CharacterBoy.png';

interface IProps {
  /**
   * @description 标题
   */
  text: string;
  /**
   * @description 样式
   */
  styles?: React.CSSProperties;
}

function NotFound({ text, styles }: IProps) {
  const history = useHistory();

  return (
    <div style={styles}>
      <div styleName="title">this is title</div>
      <div styleName="example">{text}</div>
      <div
        styleName="goto"
        onClick={() => {
          history.push('doc-center');
        }}
      >
        goto doc center {global.location.search}
      </div>
      <img src={Boy} />
    </div>
  );
}
export default NotFound;
