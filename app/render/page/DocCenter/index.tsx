import React, { useState, useEffect } from 'react';
import './index.less';
import store, { KEYS } from 'app/main/store';
import { useHistory } from 'react-router';

interface DocCenterProps {
  docCenterRoot: string | undefined;
}

function OpenDocCenterView() {
  return <div>open doc center</div>;
}

function DocCenterView({ docCenterRoot }: DocCenterProps) {
  if (!docCenterRoot) {
    return <OpenDocCenterView />;
  }
  return <div>{docCenterRoot}</div>;
}

function DocCenter() {
  const history = useHistory();
  const [docCenterPath, setDocCenterPath] = useState(store.get(KEYS.ROOT));

  const unsubscribe = store.onDidChange(KEYS.ROOT, (newVal: string | undefined, oldVal: string | undefined) => {
    // 反应太慢了
    console.log('>>> >>>old: ', oldVal);
    console.log('>>> >>>new: ', newVal);
    setDocCenterPath(store.get(KEYS.ROOT));
  });

  useEffect(() => {
    // 函数的第二个参数表明依赖改变才会调用， 滞空则表明只会调用一次，因此可以用来模拟 monut 和 unmount
    // return =>  componentWillUnmount
    return () => {
      console.log('will unmount');
      unsubscribe();
    };
  }, []);

  function openGuide(filename: string) {
    history.push(`/guide/${filename}`);
  }

  return <DocCenterView docCenterRoot={docCenterPath} />;
}
export default DocCenter;
